package co.com.porvenir.certificado.circuit.breaker.api;

import org.osgi.service.component.annotations.Component;

import co.com.porvenir.certificado.circuit.breaker.CertificadoCircuitBreakerAPIFallback;
import feign.gson.GsonDecoder;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import io.github.resilience4j.ratelimiter.RateLimiter;

@Component(service = CertificadoCircuitBreakerAPIFactory.class)
public class CertificadoCircuitBreakerAPIFactory {

	public CertificadoCircuitBreakerAPI getAPI(String name, String url) {
		
		CircuitBreaker circuitBreaker = CircuitBreaker.ofDefaults(name);
		RateLimiter rateLimiter = RateLimiter.ofDefaults(name);

		FeignDecorators decorators = FeignDecorators.builder().withCircuitBreaker(circuitBreaker)
				.withRateLimiter(rateLimiter).withFallback(new CertificadoCircuitBreakerAPIFallback()).build();
		
		return Resilience4jFeign.builder(decorators).target(CertificadoCircuitBreakerAPI.class, url);
	}
}
