package co.com.porvenir.certificado.circuit.breaker.api;

import com.liferay.portal.kernel.json.JSONObject;

import co.com.porvenir.certificado.circuit.breaker.client.Certificado;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

public interface CertificadoCircuitBreakerAPI {

	@RequestLine("POST /api/certificadoAfiliacion{tipoIdentificacion},{numeroIdentificacion}")
	@Headers("Content-Type: {headers}")
	JSONObject getAll(@Param("headers")String headers ,@Param("tipoIdentificacion")String tipoIdentificacion, @Param("numeroIdentificacion") String identificacion );

	
	@RequestLine("GET /pdf/{id}")
	Certificado getByTicker(@Param("id") String ticker);
}
