package co.com.porvenir.certificado.circuit.breaker.client;

public class Certificado {

	private String url;
	private int codeStatus;
	

	Certificado() {
		super();
	}

	public Certificado(String company, int codeStatus) {
		super();
		this.url = company;
		this.codeStatus = codeStatus;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getCodeStatus() {
		return codeStatus;
	}

	public void setCodeStatus(int codeStatus) {
		this.codeStatus = codeStatus;
	}

		
}
