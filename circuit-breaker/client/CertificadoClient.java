package co.com.porvenir.certificado.circuit.breaker.client;

import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;

import co.com.porvenir.certificado.circuit.breaker.CertificadoCircuitBreakerAPITimeLimiterService;
import co.com.porvenir.certificado.circuit.breaker.api.CertificadoCircuitBreakerAPI;
import co.com.porvenir.certificado.circuit.breaker.api.CertificadoCircuitBreakerAPIFactory;
import co.com.porvenir.certificado.circuit.breaker.configuration.CertificadoCircuitBreakerAPIConfiguration;

@Component(immediate = true, property = {}, service = CertificadoClient.class)
@SuppressWarnings("unchecked")
public class CertificadoClient {

	private final String BACKEND_NAME = "descarga-certificado";
	private volatile CertificadoCircuitBreakerAPIConfiguration configuration;
	private CertificadoCircuitBreakerAPI certificadoCircuitBreakerAPI;

	@Reference
	private CertificadoCircuitBreakerAPIFactory certificadoCircuitBreakerAPIFactory;

	@Reference
	private CertificadoCircuitBreakerAPITimeLimiterService certificadoCircuitBreakerAPITimeLimiterService;

	@Activate
	@Modified
	public void activate(Map<String, Object> properties) {
		configuration = ConfigurableUtil.createConfigurable(CertificadoCircuitBreakerAPIConfiguration.class, properties);

		certificadoCircuitBreakerAPI = certificadoCircuitBreakerAPIFactory.getAPI(BACKEND_NAME, configuration.url());
	}

	public List<Certificado> getAll(String header, String tipoId, String identificacion) {
		CompletableFuture<List<Certificado>> result = certificadoCircuitBreakerAPITimeLimiterService.getCompletableFuture(BACKEND_NAME,
				configuration.timeout(), (() -> certificadoCircuitBreakerAPI.getAll(header, tipoId, identificacion)));

		try {
			return result.get();
		} catch (InterruptedException | ExecutionException e) {
			System.out.println("Error: "+e.getMessage());
			return Collections.emptyList();
		}
	}

//	public Certificado getByTicker(String ticker) {
//		CompletableFuture<Certificado> result = certificadoCircuitBreakerAPITimeLimiterService.getCompletableFuture(BACKEND_NAME,
//				configuration.timeout(), (() -> certificadoCircuitBreakerAPI.getByTicker(ticker)));
//
//		try {
//			return result.get();
//		} catch (InterruptedException | ExecutionException e) {
//			System.out.println(e.getMessage());
//			return null;
//		}
//	}
}
