package co.com.porvenir.certificado.circuit.breaker;

import com.liferay.portal.kernel.json.JSON;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;

import java.util.Collections;
import java.util.List;

import co.com.porvenir.certificado.circuit.breaker.api.CertificadoCircuitBreakerAPI;
import co.com.porvenir.certificado.circuit.breaker.client.Certificado;

public class CertificadoCircuitBreakerAPIFallback implements CertificadoCircuitBreakerAPI	 {

	@Override
	public JSONObject getAll(String headers, String tipoIdenti, String identificacion) {
		JSONObject responseFallback=JSONFactoryUtil.createJSONObject();
		System.out.println("getAll");
		responseFallback.put("statusCode", "500");
		return responseFallback;
	}

	@Override
	public Certificado getByTicker(String ticker) {
		return null;
	}
}
