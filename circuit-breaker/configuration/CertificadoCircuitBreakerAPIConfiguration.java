package co.com.porvenir.certificado.circuit.breaker.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(
	category = "certificado-configuration",
	scope = ExtendedObjectClassDefinition.Scope.SYSTEM
)
@Meta.OCD(
	id = "co.com.porvenir.certificado.circuit.breaker.configuration.CertificadoCircuitBreakerAPIConfiguration",
	localization = "content/Language", 
	name = "certificado-configuration"
)
public interface CertificadoCircuitBreakerAPIConfiguration {

	@Meta.AD(
		description = "certificado-url",
		name = "certificado-url",
		required = false,
		deflt="https://lafuncion.azurewebsites.net"
	)
	public String url();
	
	@Meta.AD(
		description = "certificado-timeout",
		name = "certificado-timeout",
		required = false,
		deflt="100000"
	)
	public long timeout();
}
