package co.com.porvenir.certificado.circuit.breaker.configuration;

import org.osgi.service.component.annotations.Component;

import com.liferay.configuration.admin.category.ConfigurationCategory;

@Component(service = ConfigurationCategory.class)
public class CertificadoCircuitBreakerConfigurationCategory implements ConfigurationCategory {

	@Override
	public String getCategoryKey() {
		return "stockader-configuration";
	}

	@Override
	public String getCategorySection() {
		return "stockader";
	}
}
